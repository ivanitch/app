<?php

declare(strict_types=1);

require_once __DIR__.'/vendor/autoload.php';

class I
{
}

final class NoClassException extends LogicException
{
}

function _class_exists(string $class): bool
{
    $loader = static function (string $class): void {
        if (!class_exists($class, false)) {
            class_alias(I::class, $class);
        }
    };

    try {
        spl_autoload_register($loader);

        return class_exists($class);
    } catch (NoClassException $exception) {
        return true;
    } finally {
        spl_autoload_unregister($loader);
    }
}

var_dump(_class_exists(App\C::class));
